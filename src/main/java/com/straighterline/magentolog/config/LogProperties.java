package com.straighterline.magentolog.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("log")
@Data
public class LogProperties {

    private String topic;

    private String restProxyUrl;

    private Long restProxySchemaId;

    private int sleepTime;

    private int printInterval;

}
