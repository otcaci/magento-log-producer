package com.straighterline.magentolog.config;

import com.straighterline.magentolog.client.MagentoLogClientApi;
import com.straighterline.magentolog.service.MagentoLogServiceApi;
import com.straighterline.magentolog.service.impl.MagentoLogKafkaServiceApiImpl;
import com.straighterline.magentolog.service.impl.MagentoLogServiceApiImpl;
import com.straighterline.restproxy.MagentoLog;
import feign.Feign;
import feign.Logger;
import feign.gson.GsonDecoder;
import feign.gson.GsonEncoder;
import feign.httpclient.ApacheHttpClient;
import feign.slf4j.Slf4jLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaTemplate;

@Configuration
@EnableConfigurationProperties(LogProperties.class)
public class MagentoLogConfiguration {

    @Autowired
    private LogProperties properties;

    @Autowired
    private KafkaTemplate<String, MagentoLog> template;

    @Bean
    public MagentoLogClientApi magentoLogClientApi() {
        return Feign.builder()
                .client(new ApacheHttpClient())
                .encoder(new GsonEncoder())
                .decoder(new GsonDecoder())
                .logLevel(Logger.Level.NONE)
                .logger(new Slf4jLogger(MagentoLogClientApi.class))
                .target(MagentoLogClientApi.class, properties.getRestProxyUrl());
    }

    @Bean
    public MagentoLogServiceApi magentoLogServiceApi() {
        return new MagentoLogServiceApiImpl(magentoLogClientApi(), properties);
    }

    @Bean
    public MagentoLogServiceApi magentoLogKafkaServiceApi() {
        return new MagentoLogKafkaServiceApiImpl(template, properties);
    }

}
