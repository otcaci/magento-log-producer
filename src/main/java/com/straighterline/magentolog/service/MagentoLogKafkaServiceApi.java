package com.straighterline.magentolog.service;

public interface MagentoLogKafkaServiceApi {

    void readLogFile(String filePath);
}
