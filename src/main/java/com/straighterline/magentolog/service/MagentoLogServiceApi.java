package com.straighterline.magentolog.service;

public interface MagentoLogServiceApi {

    void readLogFile(String filePath);
}
