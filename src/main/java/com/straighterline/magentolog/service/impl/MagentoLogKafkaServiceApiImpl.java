package com.straighterline.magentolog.service.impl;

import com.straighterline.magentolog.config.LogProperties;
import com.straighterline.magentolog.service.MagentoLogServiceApi;
import com.straighterline.restproxy.MagentoLog;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

@Slf4j
@AllArgsConstructor
public class MagentoLogKafkaServiceApiImpl implements MagentoLogServiceApi {

    private KafkaTemplate<String, MagentoLog> kafkaTemplate;

    private final LogProperties properties;

    @Override
    public void readLogFile(String filePath) {
        log.debug("Start reading log file {}", filePath);
        try {
            File file = new File(filePath);
            long filePointer = 0;
            int processedLines = 0;

            while (true) {
                if (file.length() > filePointer) {
                    RandomAccessFile raf = new RandomAccessFile(file, "r");
                    raf.seek(filePointer);
                    String line;

                    while ((line = raf.readLine()) != null) {
                        MagentoLog data = new MagentoLog();
                        data.setMessage(line);
                        kafkaTemplate.send(properties.getTopic(), data);

                        processedLines++;
                        if (processedLines % properties.getPrintInterval() == 0) {
                            log.debug("Processed {} lines", processedLines);
                        }
                    }
                    filePointer = raf.getFilePointer();
                    raf.close();
                }

                Thread.sleep(properties.getSleepTime());
            }
        } catch (IOException | InterruptedException e) {
            log.error("An error has been occurred: " + e);
        }
    }
}
