package com.straighterline.magentolog.service.impl;

import com.straighterline.magentolog.client.MagentoLogClientApi;
import com.straighterline.magentolog.client.view.RestProxyPayload;
import com.straighterline.magentolog.config.LogProperties;
import com.straighterline.magentolog.service.MagentoLogServiceApi;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

@Slf4j
@AllArgsConstructor
public class MagentoLogServiceApiImpl implements MagentoLogServiceApi {

    private final MagentoLogClientApi clientApi;

    private final LogProperties properties;

    @Override
    public void readLogFile(String filePath) {
        log.debug("Start reading log file {}", filePath);
        try {
            File file = new File(filePath);
            long filePointer = 0;
            int processedLines = 0;

            while (true) {
                if (file.length() > filePointer) {
                    RandomAccessFile raf = new RandomAccessFile(file, "r");
                    raf.seek(filePointer);
                    String line;

                    while ((line = raf.readLine()) != null) {
                        clientApi.send(RestProxyPayload.of(properties.getRestProxySchemaId(), line));

                        processedLines++;
                        if (processedLines % properties.getPrintInterval() == 0) {
                            log.debug("Processed {} lines", processedLines);
                        }
                    }
                    filePointer = raf.getFilePointer();
                    raf.close();
                }

                Thread.sleep(properties.getSleepTime());
            }
        } catch (IOException | InterruptedException e) {
            log.error("An error has been occurred: " + e);
        }
    }
}
