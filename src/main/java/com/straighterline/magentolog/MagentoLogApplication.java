package com.straighterline.magentolog;

import com.straighterline.magentolog.exception.MagentoLogException;
import com.straighterline.magentolog.service.MagentoLogServiceApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MagentoLogApplication implements CommandLineRunner {

    @Autowired
    private MagentoLogServiceApi magentoLogKafkaServiceApi;

    public static void main(String[] args) {
        SpringApplication.run(MagentoLogApplication.class, args);
    }

    @Override
    public void run(String... args) {
        if (args.length == 0) {
            throw new MagentoLogException("FileName is missing!");
        }
        magentoLogKafkaServiceApi.readLogFile(args[0]);
    }

}
