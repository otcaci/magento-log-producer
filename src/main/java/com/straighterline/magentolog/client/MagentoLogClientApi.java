package com.straighterline.magentolog.client;

import com.straighterline.magentolog.client.view.RestProxyPayload;
import feign.Headers;
import feign.RequestLine;

public interface MagentoLogClientApi {

    @RequestLine("POST")
    @Headers({"Content-Type: application/vnd.kafka.avro.v2+json"})
    void send(RestProxyPayload payload);
}
