package com.straighterline.magentolog.client.view;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RestProxyRecord<K, T>{

    private K key;

    private T value;

}
