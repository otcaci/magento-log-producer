package com.straighterline.magentolog.client.view;

import com.google.gson.annotations.SerializedName;
import com.straighterline.restproxy.MagentoLog;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Arrays;
import java.util.List;

@Data
@AllArgsConstructor
public class RestProxyPayload {

    @SerializedName("key_schema_id")
    private Long keySchemaId;

    @SerializedName("value_schema_id")
    private Long valueSchemaId;

    private List<RestProxyRecord> records;

    public static RestProxyPayload of(Long valueSchemaId, String line){
        MagentoLog log = new MagentoLog();
        log.setMessage(line);
        return new RestProxyPayload(null, valueSchemaId, Arrays.asList(new RestProxyRecord(null, log)));
    }
}
