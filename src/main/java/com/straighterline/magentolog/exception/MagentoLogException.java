package com.straighterline.magentolog.exception;

public class MagentoLogException extends RuntimeException {

    public MagentoLogException() {
    }

    public MagentoLogException(String message) {
        super(message);
    }

    public MagentoLogException(String message, Throwable cause) {
        super(message, cause);
    }

    public MagentoLogException(Throwable cause) {
        super(cause);
    }
}
